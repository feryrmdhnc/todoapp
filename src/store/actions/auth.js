import axios from 'axios';
import * as type from "./types";
import Swal from 'sweetalert2';

const baseUrl = process.env.REACT_APP_API

export const postLogin = (body) => async dispatch => {
    try {
        const res = await axios.post(`${baseUrl}/user/login`, body, {
            headers: {
                "Content-Type": "application/json"
            }
        })
        if (res.status === 200) {
            localStorage.setItem('Token', res.data.token)
            dispatch({
                type: type.SUCCESS_LOGIN
            })
        }
    }
    catch (err) {
        dispatch({
            type: type.FAILED_LOGIN,
            payload: err.response.data
        })
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: err.response.data,
        })
    }
}

export const postRegister = (body) => async dispatch => {
    try {
        const res = await axios.post(`${baseUrl}/user/register`, body, {
            headers: {
                "Content-Type": "application/json"
            }
        })
        if (res.status === 201) {
            localStorage.setItem('Token', res.data.token)
            dispatch({
                type: type.SUCCESS_REGIS
            })
        }
    }
    catch (err) {
        dispatch({
            type: type.FAILED_REGIS,
            payload: err.response.data
        })
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: err.response.data,
        })
    }
}

export const postLogout = (body) => async dispatch => {
    // body nya none, krn post tapi d kosongkan saja
    try {
        const res = await axios.post(`${baseUrl}/user/logout`, body, {
            headers: {
                Authorization: localStorage.getItem('Token')
            }
        })
        dispatch({
            type: type.LOGOUT,
            payload: res.data
        })
    }
    catch (err) {
        console.log(err);
        dispatch({
            type: "ERROR_LOGOUT",
            payload: err.response.data
        })
    }
}

export const getProfile = () => async dispatch => {
    try {
        const res = await axios.get(`${baseUrl}/user/me`, {
            headers: {
                Authorization: localStorage.getItem('Token')
            }
        })
        if (res.status === 200) {
            dispatch({
                type: type.SUCCESS_GET_PROFILE,
                payload: res.data
            })
        }
    }
    catch (err) {
        dispatch({
            type: type.FAILED_GET_PROFILE,
            payload: err.response.data
        })
    }
}