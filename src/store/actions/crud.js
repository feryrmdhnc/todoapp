import axios from 'axios';
import * as type from "./types";
import Swal from 'sweetalert2';

const baseUrl = process.env.REACT_APP_API

export const getAllTask = () => async dispatch => {
    try {
        const res = await axios.get(`${baseUrl}/task`, {
            headers: {
                Authorization: localStorage.getItem('Token'),
                "Content-Type": "application/json"
            }
        })
        if (res.status === 200) {
            dispatch({
                type: type.GET_ALL_TASK,
                payload: res.data.data
            })
        }
    }
    catch (err) {
        dispatch({
            type: type.GET_ALL_TASK_FAILED,
            payload: err.response.data
        })
    }
}

export const postAddTask = (body) => async dispatch => {
    try {
        const res = await axios.post(`${baseUrl}/task`, body, {
            headers: {
                Authorization: localStorage.getItem('Token'),
                "Content-Type": "application/json"
            }
        })
        if (res.status === 201) {
            dispatch({
                type: type.ADD_TASK_SUCCESS
            })
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: 'Success Add task',
            })
        }
    }
    catch (err) {
        dispatch({
            type: type.ADD_TASK_FAILED,
            payload: err.response
        })
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Error add task',
        })
    }
}

export const editTasks = (_id, body) => async dispatch => {
    try {
        const res = await axios.put(`${baseUrl}/task/${_id}`, body, {
            headers: {
                Authorization: localStorage.getItem('Token'),
                "Content-Type": "application/json"
            }
        })
        if (res.status === 200) {
            dispatch({
                type: type.EDIT_TASK_SUCCESS,
                payload: res.success
            })
        }
    }
    catch (err) {
        dispatch({
            type: type.EDIT_TASK_FAILED,
            payload: err.response
        })
    }
}

export const delTask = (_id) => async dispatch => {
    try {
        const res = await axios.delete(`${baseUrl}/task/${_id}`, {
            headers: {
                Authorization: localStorage.getItem('Token'),
                "Content-Type": "application/json"
            }
        })
        if (res.status === 200) {
            dispatch({
                type: type.SUCCESS_DELETE_TASK,
                payload: _id
            })
        }
    }
    catch (err) {
        dispatch({
            type: type.FAILED_DELETE_TASK,
            payload: err.response.data
        })
    }
}

export const getImage = (id) => async dispatch => {
    try {
        const res = await axios.get(`${baseUrl}/user/${id}/avatar`)
        if (res.status === 200) {
            dispatch({
                type: type.GET_IMAGE_PROFILE,
                payload: res.data
            })
        }
    }
    catch (err) {
        dispatch({
            type: "GET_IMAGE_PROFILE_FAILED",
            payload: err.response.data
        })
    }
}