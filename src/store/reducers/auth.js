import * as type from "../actions/types";

const initialState = {
    token: null,
    isAuthenticate: false,
    isLoading: true,
    profile: {}
}

export const auth = (state = initialState, action) => {

    switch (action.type) {
        default:
            return {
                ...state
            }
        case type.SUCCESS_LOGIN:
            return {
                ...state,
                token: localStorage.getItem('Token'),
                isAuthenticate: true,
            }
        case type.FAILED_LOGIN:
            return {
                ...state,
                isAuthenticate: false
            }
        case type.SUCCESS_REGIS:
            return {
                ...state,
                isAuthenticate: true,
            }
        case type.FAILED_REGIS:
            return {
                ...state,
                isAuthenticate: false
            }
        case type.LOGOUT:
            return {
                ...state,
                isAuthenticate: false
            }
        case type.SUCCESS_GET_PROFILE:
            return {
                ...state,
                isAuthenticate: true,
                profile: action.payload
            }
        case type.FAILED_GET_PROFILE:
            return {
                ...state,
                isAuthenticate: false
            }
        case type.ADD_TASK_SUCCESS:
            return {
                ...state,
                isAuthenticate: true,
            }
        case type.ADD_TASK_FAILED:
            return {
                ...state,
                isAuthenticate: false
            }
    }

}