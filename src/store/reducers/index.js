import { combineReducers } from 'redux';
import { auth } from './auth';
import { crud } from './crud';

const rootReducers = combineReducers({
    auth,
    crud
})

export default rootReducers;