import * as type from "../actions/types";

const initialState = {
    isAuthenticate: false,
    isLoading: true,
    task: [],
    image: []
}

export const crud = (state = initialState, action) => {

    switch (action.type) {
        default:
            return {
                ...state
            }
        case type.GET_ALL_TASK:
            return {
                ...state,
                isAuthenticate: true,
                task: action.payload
            }
        case type.GET_ALL_TASK_FAILED:
            return {
                ...state,
                isAuthenticate: false,
            }
        case type.SUCCESS_DELETE_TASK:
            return {
                ...state,
                isAuthenticate: true,
                task: state.task.filter((item) => item._id !== action.payload)
            }
        case type.FAILED_DELETE_TASK:
            return {
                ...state,
            }
        case type.GET_IMAGE_PROFILE:
            return {
                ...state,
                isLoading: false,
                image: action.payload
            }
        case type.EDIT_TASK_SUCCESS:
            return {
                ...state,
                isAuthenticate: false,
                isLoading: false
            }
        case type.EDIT_TASK_FAILED:
            return {
                ...state,
            }
    }

}