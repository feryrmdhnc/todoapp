import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';


const PrivateRoutes = (props) => {
    const isAuthenticated = useSelector(state => state.auth.isAuthenticate)
    let { component: Component, ...rest } = props
    return (
        <Route
            {...rest}
            render={props => localStorage.getItem('Token') || isAuthenticated ? (
                <Component {...props} />
            ) : (
                <Redirect to={{ pathname: "/login" }} />
            )}
        />
    )
}

export default PrivateRoutes;
