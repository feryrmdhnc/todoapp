import HeaderApp from "components/HeaderApp";
import { lazy, Suspense } from "react";
import { HelmetProvider } from "react-helmet-async";
import { Route, Switch, useRouteMatch } from "react-router";

const HomePage = lazy(() => import("views/HomePage"))

const MainRoutes = () => {
    let { path } = useRouteMatch()

    return (
        <>
            <HelmetProvider>
                <Suspense fallback={<div></div>}>
                    <HeaderApp />
                    <Switch>
                        <Route path={`${path}`} component={HomePage} exact />
                    </Switch>
                    {/* <footer></footer> */}
                </Suspense>
            </HelmetProvider>
        </>
    )
}

export default MainRoutes;