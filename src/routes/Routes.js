import { lazy, Suspense } from "react";
import { HelmetProvider } from "react-helmet-async";
import MainRoutes from "./MainRoutes";
import { Route, Switch } from "react-router";
import PrivateRoutes from "./PrivateRoutes";

const Login = lazy(() => import("views/Login"))
const Register = lazy(() => import("views/Register"))

const Routes = () => {
    return (
        <>
            <HelmetProvider>
                <Suspense fallback={<div></div>}>
                    <Switch>
                        <Route path="/login" component={Login} exact />
                        <Route path="/regis" component={Register} exact />
                        <PrivateRoutes path="/home" component={MainRoutes} />
                        <Route path="/home" component={MainRoutes} exact />
                    </Switch>
                </Suspense>
            </HelmetProvider>
        </>
    )
}

export default Routes;