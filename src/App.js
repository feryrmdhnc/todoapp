import { Helmet } from 'react-helmet';
import { HelmetProvider } from 'react-helmet-async';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Routes from 'routes/Routes';

import './App.css';

function App() {
  return (
    <BrowserRouter>
      <HelmetProvider>
        <Helmet>
          <meta name="description" content="Simple project app todo list" />
          <meta name="robots" content="index, follow" />
          <meta name="keywords" content="Todo list, Todo app, TodoApp, todolist, todoapp, " />
          <meta name="author" content="Fery Todoapp" />
        </Helmet>
        <div className="App">
          <Switch>
            <Routes />
          </Switch>
          <Switch>
            <Route exact path="/"
              render={() => {
                return (
                  <Redirect to="/login" />
                )
              }}
            />
          </Switch>
        </div>
      </HelmetProvider>
    </BrowserRouter>

  );
}

export default App;
