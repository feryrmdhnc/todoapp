import Dropdown from 'react-bootstrap/Dropdown';
import { useDispatch, useSelector } from 'react-redux';
// import { getImage } from 'store/actions/crud';
import { useHistory } from 'react-router';
import { postLogout } from 'store/actions/auth';
import Swal from 'sweetalert2';

import 'assets/style/header.scss';

const HeaderApp = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const profile = useSelector((state) => state.auth.profile)
    // const image = useSelector((state) => state.auth.image)

    const alerts = () => {
        Swal.fire({
            icon: 'info',
            title: 'Info',
            text: 'Page not available',
        })
    }

    const logout = async (e) => {
        e.preventDefault()
        await dispatch(postLogout())
        await localStorage.removeItem('Token')
        history.replace("/login")
    }

    return (
        <>
            <div className="headers">
                <div className="title">
                    <h3>Todo List</h3>
                </div>
                <div className="profile-wrapper">
                    <div className="photo">
                        <img src={'https://thumbs.dreamstime.com/b/default-avatar-photo-placeholder-profile-picture-default-avatar-photo-placeholder-profile-picture-eps-file-easy-to-edit-125707135.jpg'} alt="photos" />
                    </div>
                    <Dropdown className="menu">
                        <Dropdown.Toggle id="dropdown-basic"></Dropdown.Toggle>
                        <Dropdown.Menu>
                            <h5>{profile.name}</h5>
                            <Dropdown.Item onClick={alerts}>Edit Profile</Dropdown.Item>
                            <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
        </>
    )
}

export default HeaderApp;