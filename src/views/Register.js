import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { Button, Form, Spinner } from "react-bootstrap";
import { Helmet } from "react-helmet";
import { Link, useHistory } from "react-router-dom";
import { postRegister } from "store/actions/auth";

import Logo from 'assets/image/regis.png';
import 'assets/style/regis.scss';
import 'assets/style/responsive-outside.scss';


const Register = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const isAuthenticate = useSelector((state) => state.auth.isAuthenticate)
    const [loading, setLoading] = useState(false)
    const [body, setBody] = useState({
        name: "",
        email: "",
        password: "",
        age: ""
    })

    const handleInput = (e) => {
        setBody({
            ...body,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        if (isAuthenticate) {
            history.push("/login")
        }
    })

    const register = async (e) => {
        e.preventDefault()
        await setLoading(true)
        await dispatch(postRegister(body))
        setLoading(false)
    }

    const login = () => {
        history.push("/login")
    }

    return (
        <>
            <Helmet>
                <title>Register</title>
            </Helmet>
            <div className="regis-page">
                <div className="login-wrapper">
                    <h3>Hello My Friend!</h3>
                    <div className="paragraph">
                        <p>Do you have an account?</p>
                        <p>Come on, login immediately to manage your tasks!</p>
                    </div>
                    <div className="wrapper-btn-login">
                        <Link to="/login">
                            <Button className="btn-login">Login</Button>
                        </Link>
                    </div>
                </div>
                <div className="form-wrapper">
                    <h2>Register Your Account Here!</h2>
                    <div className="logo">
                        <img src={Logo} alt="logo-regis" />
                    </div>
                    <div className="form-regis">
                        <Form onSubmit={register}>
                            <Form.Group controlId="formBasicName">
                                <Form.Control type="text" name="name" placeholder="Name" value={body.name} onChange={handleInput} />
                            </Form.Group>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Control type="email" name="email" placeholder="Email" value={body.email} onChange={handleInput} />
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Control type="password" name="password" placeholder="Password" value={body.password} onChange={handleInput} />
                            </Form.Group>
                            <Form.Group controlId="formBasicAge">
                                <Form.Control type="text" name="age" placeholder="Age" value={body.age} onChange={handleInput} />
                            </Form.Group>
                            <Form.Group controlId="formBasicCheckbox">
                                <Form.Check className="check" type="checkbox" label="Remember me" />
                            </Form.Group>
                            {loading ?
                                <Button className="btn-regis" variant="primary" type="submit" disabled>
                                    <Spinner animation="border" size="sm" />
                                </Button>
                                :
                                <Button className="btn-regis" variant="primary" type="submit">Register</Button>
                            }
                        </Form>
                    </div>
                    <p className="login-mobile">Have an account? Login <span onClick={login}>here!</span></p>
                </div>
            </div>
        </>
    )
}

export default Register;