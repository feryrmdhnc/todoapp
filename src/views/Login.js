import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Button, Spinner } from 'react-bootstrap';
import Form from 'react-bootstrap/Form'
import { Link, useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";
import { postLogin } from '../store/actions/auth';
import { useSelector } from "react-redux";

import Logos from 'assets/image/todos.png';
import '../assets/style/login.scss';
import 'assets/style/responsive-outside.scss';

const Login = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const isAuthenticate = useSelector((state) => state.auth.isAuthenticate)
    const [loading, setLoading] = useState(false)
    const [body, setBody] = useState({
        email: "",
        password: ""
    })

    const handleInput = (e) => {
        setBody({
            ...body,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        if (isAuthenticate) {
            history.push("/home")
        }
    })

    const login = async (e) => {
        e.preventDefault()
        await setLoading(true)
        await dispatch(postLogin(body))
        setLoading(false)
    }

    const regis = () => {
        history.push("/regis")
    }

    return (
        <>
            <Helmet>
                <title>Login</title>
            </Helmet>
            <div className="login-page">
                <div className="regis-wrapper">
                    <h3>Hello My Friend!</h3>
                    <div className="paragraph">
                        <p>Enter your personal details</p>
                        <p>and start your journey with us!</p>
                        <p>You will find the benefit of</p>
                        <p>recording your duties.</p>
                    </div>
                    <div className="wrapper-btn-regis">
                        <Link to="/regis">
                            <Button className="btn-regis">Register</Button>
                        </Link>
                    </div>
                </div>
                <div className="form-wrapper">
                    <h2>Login to Manage Your Task</h2>
                    <div className="logo">
                        <img src={Logos} alt="logo-todo" />
                    </div>
                    <div className="form-login">
                        <Form onSubmit={login}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Control type="email" placeholder="Enter email" name="email" value={body.email} onChange={handleInput} />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Control type="password" placeholder="Password" name="password" value={body.password} onChange={handleInput} />
                            </Form.Group>
                            <Form.Group controlId="formBasicCheckbox">
                                <Form.Check className="check" type="checkbox" label="Remember me" />
                            </Form.Group>
                            {loading ?
                                <Button className="btn-login" variant="primary" type="submit" disabled>
                                    <Spinner animation="border" size="sm" />
                                </Button>
                                :
                                <Button className="btn-login" variant="primary" type="submit">Login</Button>
                            }
                        </Form>
                    </div>
                    <p className="regis-mobile">New User? Register <span onClick={regis}>here!</span></p>
                </div>
            </div>
        </>
    )
}

export default Login;