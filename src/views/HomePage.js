import { Helmet } from 'react-helmet';
import { Button, Form, Spinner, Modal } from "react-bootstrap";
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getProfile } from 'store/actions/auth';
import { delTask, getAllTask, postAddTask, editTasks } from 'store/actions/crud';

import 'assets/style/homepage.scss';
import 'assets/style/responsive-inside.scss';

const HomePage = () => {
    const dispatch = useDispatch()
    const Profile = useSelector((state) => state.auth.profile)
    const Task = useSelector((state) => state.crud.task)
    const [loading, setLoading] = useState(false)
    const [loadingDel, setLoadingDel] = useState(false)
    const [show, setShow] = useState(false)
    const [showEdit, setShowEdit] = useState(false)
    const [body, setBody] = useState({
        description: ""
    })
    const [data, setData] = useState({
        completed: false
    })
    const [idDel, setIdDel] = useState(0)

    const handleInput = (e) => {
        setBody({
            ...body,
            [e.target.name]: e.target.value
        })
    }

    const handleChangeStatus = (e) => {
        setData({
            completed: e.target.value
        })
    }

    useEffect(() => {
        dispatch(getProfile())
        dispatch(getAllTask())
    }, [dispatch])

    let statusTask = (item) => {
        if (item.completed === true) {
            return <h5 className="completed">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                </svg> Completed
                </h5>
        } else {
            return <h5 className="not-complete">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z" />
                </svg> Not Complete
                </h5>
        }
    }

    const addTask = async (e) => {
        e.preventDefault()
        await setLoading(true)
        await dispatch(postAddTask(body))
        await setLoading(false)
        dispatch(getAllTask())
        setBody({ description: "" })
    }

    const handleShowEdit = () => setShowEdit(true)
    const handleCloseEdit = () => setShowEdit(false)
    const editTask = async (e, _id) => {
        e.preventDefault()
        await setLoading(true)
        await dispatch(editTasks(_id, data))
        await setLoading(false)
        await setShowEdit(false)
        dispatch(getAllTask())
    }

    const handleShowDel = (_id) => {
        setIdDel(_id)
        setShow(true, _id)
    }
    const handleCloseDel = () => setShow(false)
    const deleteTask = async (_id) => {
        await setLoadingDel(true)
        await dispatch(delTask(_id))
        await setLoadingDel(false)
        await setShow(false)
        dispatch(getAllTask())
    }

    return (
        <>
            <Helmet>
                <title>{process.env.REACT_APP_NAME}</title>
            </Helmet>
            <div className="home">
                {/* Profile telat diget maka dr itu pke ternary utk diakali */}
                {Profile ?
                    <h5>Hello {Profile.name} Add your task here!</h5>
                    :
                    <h5>Hello Add your task here!</h5>
                }
                <div className="form-wrapper">
                    <Form className="form-task" onSubmit={addTask}>
                        <Form.Group controlId="formBasicTask" className="text">
                            <Form.Control type="text" name="description" value={body.description} onChange={handleInput} placeholder="Add this" />
                        </Form.Group>
                        {loading ?
                            <Button className="btn-add-task" variant="primary" type="submit">
                                <Spinner animation="border" size="sm" />
                            </Button>
                            :
                            <Button className="btn-add-task" variant="primary" type="submit">
                                Add
                            </Button>
                        }
                    </Form>
                </div>
                <div className="task-wrapper">
                    {Task.map((item) => (
                        <div key={item._id} className="card-task">
                            <div className="title-menu">
                                <div className="status">
                                    <h4>{item.description}</h4>
                                    {statusTask(item)}
                                </div>
                                <div className="btn-action">
                                    <Button onClick={handleShowEdit}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-fill" viewBox="0 0 16 16">
                                            <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                        </svg>
                                    </Button>
                                    <Button onClick={() => handleShowDel(item._id)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill" viewBox="0 0 16 16">
                                            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z" />
                                        </svg>
                                    </Button>
                                </div>
                            </div>
                            <p>{item.createdAt.substr(0, 10)}&nbsp;&nbsp;&nbsp;&nbsp;<span>{item.createdAt.substr(11, 8)}</span></p>

                            <Modal show={showEdit} onHide={handleCloseEdit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Edit Task</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form>
                                        <Form.Group controlId="exampleForm.ControlSelect1">
                                            <Form.Label>Completed</Form.Label>
                                            <Form.Control as="select" onChange={handleChangeStatus}>
                                                <option value={false}>Not Complete</option>
                                                <option value={true}>Completed</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleCloseDel}>
                                        Cancel
                                       </Button>
                                    {loading ?
                                        <Button variant="primary">
                                            <Spinner animation="border" size="sm" />
                                        </Button>
                                        :
                                        <Button variant="primary" onClick={(e) => editTask(e, item._id)}>
                                            OK
                                       </Button>
                                    }
                                </Modal.Footer>
                            </Modal>
                        </div>
                    ))}
                </div>

                <Modal show={show} onHide={handleCloseDel}>
                    <Modal.Header closeButton>
                        <Modal.Title>Delete</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure ?</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleCloseDel}>
                            Cancel
                        </Button>
                        {loadingDel ?
                            <Button variant="primary">
                                <Spinner animation="border" size="sm" />
                            </Button>
                            :
                            <Button variant="primary" onClick={() => deleteTask(idDel)}>
                                OK
                            </Button>
                        }
                    </Modal.Footer>
                </Modal>

            </div>
        </>
    )
}

export default HomePage;